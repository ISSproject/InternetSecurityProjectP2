﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InternetSecurityProject
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Gnostice.Documents.Framework.ActivateLicense("AF85-D483-C039-24F2-B995-4F0C-1BB9-A382-08A3-70DF-943B-00C4");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(LoginForm.GetInstance());
            //Application.Run(new Client.OpenReadDoc());

        }
    }
}
