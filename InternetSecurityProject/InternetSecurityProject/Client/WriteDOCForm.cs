﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Application = Microsoft.Office.Interop.Word.Application;
using Microsoft.Office.Interop.Word;

namespace InternetSecurityProject.Client
{
    public partial class WriteDOCForm : MetroForm
    {
        InternetSecurityDCDataContext dc = new InternetSecurityDCDataContext();
        public WriteDOCForm()
        {
            InitializeComponent();
            richTextBox1.WordWrap = true;
        }

        private void metroTextButton1_Click(object sender, EventArgs e)
        {
            //Get the richTextBox1 Content
            var txt = richTextBox1.Text.ToCharArray();
            // Open word and a docx file
            var wordApplication = new Application() { Visible = false };
            var document = wordApplication.Documents.Open(NameLabel.Text, Visible: false);
            // Insert text
            var pText = document.Paragraphs.Add();
            pText.Format.SpaceAfter = 10f;
            pText.Range.Font.Size = 13;
            pText.Range.Text = String.Format(new string(txt));
            pText.Range.InsertParagraphAfter();

            // Save settings
            document.Save();
            // Close word
            wordApplication.Quit();

            /*Calculate the New Mac And Update!!*/
            var dbfile = dc.Files.FirstOrDefault(x => x.Name.Equals(NameLabel.Text));

            try
            {
                dbfile.LastUpdateDate = DateTime.Now;
                dbfile.LastUpdateUser = this.IDLabel.Text;
                dc.SubmitChanges();
                var calcMac = new Lib.CMAC();
                calcMac.CreateMAC(dbfile.Directory + @"\" + dbfile.Name);
            }
            catch (Exception ex)
            {
                MsgForm msg = new MsgForm();
                msg.Text = ex.Message.ToString();
                msg.ShowDialog();
            }

            this.Close();
        }
    }
}
