﻿namespace InternetSecurityProject.Client
{
    partial class WlcForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WlcForm));
            this.label1 = new System.Windows.Forms.Label();
            this.IDLabel = new System.Windows.Forms.Label();
            this.LogoutLink = new MetroFramework.Controls.MetroLink();
            this.filesgroupBox = new System.Windows.Forms.GroupBox();
            this.FileslistBox = new System.Windows.Forms.ListBox();
            this.userinfogroupBox = new System.Windows.Forms.GroupBox();
            this.userLabelsLabel = new System.Windows.Forms.Label();
            this.userSecurityCleareanceLabel = new System.Windows.Forms.Label();
            this.LabelL = new System.Windows.Forms.Label();
            this.labelC = new System.Windows.Forms.Label();
            this.writePicture = new System.Windows.Forms.PictureBox();
            this.readPicture = new System.Windows.Forms.PictureBox();
            this.labelWrite = new System.Windows.Forms.Label();
            this.labelRead = new System.Windows.Forms.Label();
            this.labelU = new System.Windows.Forms.Label();
            this.FileinfogroupBox = new System.Windows.Forms.GroupBox();
            this.fileLabelsLabel = new System.Windows.Forms.Label();
            this.labelS2 = new System.Windows.Forms.Label();
            this.labelL2 = new System.Windows.Forms.Label();
            this.fileSecurityCleareanceLabel = new System.Windows.Forms.Label();
            this.fileOperationsGroupBox = new System.Windows.Forms.GroupBox();
            this.operationComboBox = new MetroFramework.Controls.MetroComboBox();
            this.resultLabel = new System.Windows.Forms.Label();
            this.executeButton = new System.Windows.Forms.Button();
            this.labelResult = new System.Windows.Forms.Label();
            this.labelOperation = new System.Windows.Forms.Label();
            this.filesgroupBox.SuspendLayout();
            this.userinfogroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.writePicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.readPicture)).BeginInit();
            this.FileinfogroupBox.SuspendLayout();
            this.fileOperationsGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label1.Location = new System.Drawing.Point(345, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome ID: ";
            // 
            // IDLabel
            // 
            this.IDLabel.AutoSize = true;
            this.IDLabel.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDLabel.Location = new System.Drawing.Point(457, 13);
            this.IDLabel.Name = "IDLabel";
            this.IDLabel.Size = new System.Drawing.Size(56, 17);
            this.IDLabel.TabIndex = 1;
            this.IDLabel.Text = "UserID";
            // 
            // LogoutLink
            // 
            this.LogoutLink.FontSize = MetroFramework.MetroLinkSize.Medium;
            this.LogoutLink.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.LogoutLink.Location = new System.Drawing.Point(778, 448);
            this.LogoutLink.Name = "LogoutLink";
            this.LogoutLink.Size = new System.Drawing.Size(75, 23);
            this.LogoutLink.Style = MetroFramework.MetroColorStyle.Blue;
            this.LogoutLink.TabIndex = 2;
            this.LogoutLink.Text = "LOGOUT";
            this.LogoutLink.Theme = MetroFramework.MetroThemeStyle.Light;
            this.LogoutLink.UseSelectable = true;
            this.LogoutLink.Click += new System.EventHandler(this.LogoutLink_Click);
            // 
            // filesgroupBox
            // 
            this.filesgroupBox.Controls.Add(this.FileslistBox);
            this.filesgroupBox.Location = new System.Drawing.Point(23, 42);
            this.filesgroupBox.Name = "filesgroupBox";
            this.filesgroupBox.Size = new System.Drawing.Size(490, 411);
            this.filesgroupBox.TabIndex = 3;
            this.filesgroupBox.TabStop = false;
            this.filesgroupBox.Text = "Files";
            // 
            // FileslistBox
            // 
            this.FileslistBox.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FileslistBox.FormattingEnabled = true;
            this.FileslistBox.ItemHeight = 19;
            this.FileslistBox.Location = new System.Drawing.Point(0, 21);
            this.FileslistBox.Name = "FileslistBox";
            this.FileslistBox.Size = new System.Drawing.Size(484, 384);
            this.FileslistBox.TabIndex = 0;
            this.FileslistBox.SelectedIndexChanged += new System.EventHandler(this.FileslistBox_SelectedIndexChanged);
            // 
            // userinfogroupBox
            // 
            this.userinfogroupBox.Controls.Add(this.userLabelsLabel);
            this.userinfogroupBox.Controls.Add(this.userSecurityCleareanceLabel);
            this.userinfogroupBox.Controls.Add(this.LabelL);
            this.userinfogroupBox.Controls.Add(this.labelC);
            this.userinfogroupBox.Location = new System.Drawing.Point(532, 42);
            this.userinfogroupBox.Name = "userinfogroupBox";
            this.userinfogroupBox.Size = new System.Drawing.Size(321, 86);
            this.userinfogroupBox.TabIndex = 4;
            this.userinfogroupBox.TabStop = false;
            this.userinfogroupBox.Text = "User Info";
            // 
            // userLabelsLabel
            // 
            this.userLabelsLabel.AutoSize = true;
            this.userLabelsLabel.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userLabelsLabel.ForeColor = System.Drawing.Color.DodgerBlue;
            this.userLabelsLabel.Location = new System.Drawing.Point(87, 49);
            this.userLabelsLabel.Name = "userLabelsLabel";
            this.userLabelsLabel.Size = new System.Drawing.Size(53, 18);
            this.userLabelsLabel.TabIndex = 20;
            this.userLabelsLabel.Text = "labels";
            // 
            // userSecurityCleareanceLabel
            // 
            this.userSecurityCleareanceLabel.AutoSize = true;
            this.userSecurityCleareanceLabel.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userSecurityCleareanceLabel.ForeColor = System.Drawing.Color.Red;
            this.userSecurityCleareanceLabel.Location = new System.Drawing.Point(139, 24);
            this.userSecurityCleareanceLabel.Name = "userSecurityCleareanceLabel";
            this.userSecurityCleareanceLabel.Size = new System.Drawing.Size(90, 18);
            this.userSecurityCleareanceLabel.TabIndex = 7;
            this.userSecurityCleareanceLabel.Text = "cleareance";
            // 
            // LabelL
            // 
            this.LabelL.AutoSize = true;
            this.LabelL.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelL.Location = new System.Drawing.Point(16, 51);
            this.LabelL.Name = "LabelL";
            this.LabelL.Size = new System.Drawing.Size(49, 16);
            this.LabelL.TabIndex = 8;
            this.LabelL.Text = "Labels:";
            // 
            // labelC
            // 
            this.labelC.AutoSize = true;
            this.labelC.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelC.Location = new System.Drawing.Point(15, 27);
            this.labelC.Name = "labelC";
            this.labelC.Size = new System.Drawing.Size(118, 14);
            this.labelC.TabIndex = 7;
            this.labelC.Text = "Security Cleareance:";
            // 
            // writePicture
            // 
            this.writePicture.Image = ((System.Drawing.Image)(resources.GetObject("writePicture.Image")));
            this.writePicture.Location = new System.Drawing.Point(217, 97);
            this.writePicture.Name = "writePicture";
            this.writePicture.Size = new System.Drawing.Size(20, 20);
            this.writePicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.writePicture.TabIndex = 18;
            this.writePicture.TabStop = false;
            // 
            // readPicture
            // 
            this.readPicture.Image = global::InternetSecurityProject.Properties.Resources.approve;
            this.readPicture.Location = new System.Drawing.Point(113, 97);
            this.readPicture.Name = "readPicture";
            this.readPicture.Size = new System.Drawing.Size(20, 20);
            this.readPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.readPicture.TabIndex = 17;
            this.readPicture.TabStop = false;
            // 
            // labelWrite
            // 
            this.labelWrite.AutoSize = true;
            this.labelWrite.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWrite.Location = new System.Drawing.Point(253, 99);
            this.labelWrite.Name = "labelWrite";
            this.labelWrite.Size = new System.Drawing.Size(40, 14);
            this.labelWrite.TabIndex = 15;
            this.labelWrite.Text = "Write";
            // 
            // labelRead
            // 
            this.labelRead.AutoSize = true;
            this.labelRead.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRead.Location = new System.Drawing.Point(151, 99);
            this.labelRead.Name = "labelRead";
            this.labelRead.Size = new System.Drawing.Size(38, 14);
            this.labelRead.TabIndex = 14;
            this.labelRead.Text = "Read";
            // 
            // labelU
            // 
            this.labelU.AutoSize = true;
            this.labelU.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelU.Location = new System.Drawing.Point(15, 99);
            this.labelU.Name = "labelU";
            this.labelU.Size = new System.Drawing.Size(69, 14);
            this.labelU.TabIndex = 13;
            this.labelU.Text = "User rights:";
            // 
            // FileinfogroupBox
            // 
            this.FileinfogroupBox.Controls.Add(this.fileLabelsLabel);
            this.FileinfogroupBox.Controls.Add(this.labelS2);
            this.FileinfogroupBox.Controls.Add(this.labelL2);
            this.FileinfogroupBox.Controls.Add(this.writePicture);
            this.FileinfogroupBox.Controls.Add(this.fileSecurityCleareanceLabel);
            this.FileinfogroupBox.Controls.Add(this.readPicture);
            this.FileinfogroupBox.Controls.Add(this.labelWrite);
            this.FileinfogroupBox.Controls.Add(this.labelU);
            this.FileinfogroupBox.Controls.Add(this.labelRead);
            this.FileinfogroupBox.Location = new System.Drawing.Point(532, 149);
            this.FileinfogroupBox.Name = "FileinfogroupBox";
            this.FileinfogroupBox.Size = new System.Drawing.Size(321, 146);
            this.FileinfogroupBox.TabIndex = 5;
            this.FileinfogroupBox.TabStop = false;
            this.FileinfogroupBox.Text = "File Info";
            // 
            // fileLabelsLabel
            // 
            this.fileLabelsLabel.AutoSize = true;
            this.fileLabelsLabel.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileLabelsLabel.ForeColor = System.Drawing.Color.DodgerBlue;
            this.fileLabelsLabel.Location = new System.Drawing.Point(87, 52);
            this.fileLabelsLabel.Name = "fileLabelsLabel";
            this.fileLabelsLabel.Size = new System.Drawing.Size(53, 18);
            this.fileLabelsLabel.TabIndex = 17;
            this.fileLabelsLabel.Text = "labels";
            // 
            // labelS2
            // 
            this.labelS2.AutoSize = true;
            this.labelS2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelS2.Location = new System.Drawing.Point(15, 25);
            this.labelS2.Name = "labelS2";
            this.labelS2.Size = new System.Drawing.Size(118, 14);
            this.labelS2.TabIndex = 14;
            this.labelS2.Text = "Security Cleareance:";
            // 
            // labelL2
            // 
            this.labelL2.AutoSize = true;
            this.labelL2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelL2.Location = new System.Drawing.Point(15, 52);
            this.labelL2.Name = "labelL2";
            this.labelL2.Size = new System.Drawing.Size(49, 16);
            this.labelL2.TabIndex = 16;
            this.labelL2.Text = "Labels:";
            // 
            // fileSecurityCleareanceLabel
            // 
            this.fileSecurityCleareanceLabel.AutoSize = true;
            this.fileSecurityCleareanceLabel.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileSecurityCleareanceLabel.ForeColor = System.Drawing.Color.Red;
            this.fileSecurityCleareanceLabel.Location = new System.Drawing.Point(138, 25);
            this.fileSecurityCleareanceLabel.Name = "fileSecurityCleareanceLabel";
            this.fileSecurityCleareanceLabel.Size = new System.Drawing.Size(90, 18);
            this.fileSecurityCleareanceLabel.TabIndex = 15;
            this.fileSecurityCleareanceLabel.Text = "cleareance";
            // 
            // fileOperationsGroupBox
            // 
            this.fileOperationsGroupBox.Controls.Add(this.operationComboBox);
            this.fileOperationsGroupBox.Controls.Add(this.resultLabel);
            this.fileOperationsGroupBox.Controls.Add(this.executeButton);
            this.fileOperationsGroupBox.Controls.Add(this.labelResult);
            this.fileOperationsGroupBox.Controls.Add(this.labelOperation);
            this.fileOperationsGroupBox.Location = new System.Drawing.Point(532, 315);
            this.fileOperationsGroupBox.Name = "fileOperationsGroupBox";
            this.fileOperationsGroupBox.Size = new System.Drawing.Size(321, 115);
            this.fileOperationsGroupBox.TabIndex = 6;
            this.fileOperationsGroupBox.TabStop = false;
            this.fileOperationsGroupBox.Text = "File operations";
            // 
            // operationComboBox
            // 
            this.operationComboBox.DropDownHeight = 90;
            this.operationComboBox.Enabled = false;
            this.operationComboBox.FormattingEnabled = true;
            this.operationComboBox.IntegralHeight = false;
            this.operationComboBox.ItemHeight = 23;
            this.operationComboBox.Items.AddRange(new object[] {
            "Read",
            "Write"});
            this.operationComboBox.Location = new System.Drawing.Point(130, 19);
            this.operationComboBox.Name = "operationComboBox";
            this.operationComboBox.Size = new System.Drawing.Size(98, 29);
            this.operationComboBox.TabIndex = 5;
            this.operationComboBox.UseSelectable = true;
            // 
            // resultLabel
            // 
            this.resultLabel.AutoSize = true;
            this.resultLabel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultLabel.ForeColor = System.Drawing.Color.Red;
            this.resultLabel.Location = new System.Drawing.Point(70, 70);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(49, 16);
            this.resultLabel.TabIndex = 4;
            this.resultLabel.Text = "Result";
            // 
            // executeButton
            // 
            this.executeButton.Location = new System.Drawing.Point(240, 23);
            this.executeButton.Name = "executeButton";
            this.executeButton.Size = new System.Drawing.Size(75, 23);
            this.executeButton.TabIndex = 3;
            this.executeButton.Text = "Execute";
            this.executeButton.UseVisualStyleBackColor = true;
            this.executeButton.Click += new System.EventHandler(this.executeButton_Click);
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResult.Location = new System.Drawing.Point(6, 70);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(48, 16);
            this.labelResult.TabIndex = 2;
            this.labelResult.Text = "Result:";
            // 
            // labelOperation
            // 
            this.labelOperation.AutoSize = true;
            this.labelOperation.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOperation.Location = new System.Drawing.Point(6, 27);
            this.labelOperation.Name = "labelOperation";
            this.labelOperation.Size = new System.Drawing.Size(118, 14);
            this.labelOperation.TabIndex = 0;
            this.labelOperation.Text = "Select an operation:";
            // 
            // WlcForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(876, 483);
            this.Controls.Add(this.fileOperationsGroupBox);
            this.Controls.Add(this.FileinfogroupBox);
            this.Controls.Add(this.userinfogroupBox);
            this.Controls.Add(this.filesgroupBox);
            this.Controls.Add(this.LogoutLink);
            this.Controls.Add(this.IDLabel);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "WlcForm";
            this.filesgroupBox.ResumeLayout(false);
            this.userinfogroupBox.ResumeLayout(false);
            this.userinfogroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.writePicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.readPicture)).EndInit();
            this.FileinfogroupBox.ResumeLayout(false);
            this.FileinfogroupBox.PerformLayout();
            this.fileOperationsGroupBox.ResumeLayout(false);
            this.fileOperationsGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label IDLabel;
        private MetroFramework.Controls.MetroLink LogoutLink;
        private System.Windows.Forms.GroupBox filesgroupBox;
        private System.Windows.Forms.GroupBox userinfogroupBox;
        private System.Windows.Forms.GroupBox FileinfogroupBox;
        private System.Windows.Forms.PictureBox writePicture;
        private System.Windows.Forms.PictureBox readPicture;
        private System.Windows.Forms.Label labelWrite;
        private System.Windows.Forms.Label labelRead;
        private System.Windows.Forms.Label labelU;
        private System.Windows.Forms.Label LabelL;
        private System.Windows.Forms.Label labelC;
        private System.Windows.Forms.Label userLabelsLabel;
        private System.Windows.Forms.Label userSecurityCleareanceLabel;
        private System.Windows.Forms.Label fileLabelsLabel;
        private System.Windows.Forms.Label labelS2;
        private System.Windows.Forms.Label labelL2;
        private System.Windows.Forms.Label fileSecurityCleareanceLabel;
        private System.Windows.Forms.ListBox FileslistBox;
        private System.Windows.Forms.GroupBox fileOperationsGroupBox;
        private System.Windows.Forms.Label resultLabel;
        private System.Windows.Forms.Button executeButton;
        private System.Windows.Forms.Label labelResult;
        private System.Windows.Forms.Label labelOperation;
        private MetroFramework.Controls.MetroComboBox operationComboBox;
    }
}