﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InternetSecurityProject.Client
{
    public partial class DocViewerForm : MetroForm
    {
        public DocViewerForm()
        {
            InitializeComponent();
        }

        public void openDoc(string path)
        {
            documentViewer1.LoadDocument(path);
        }
    }
}
