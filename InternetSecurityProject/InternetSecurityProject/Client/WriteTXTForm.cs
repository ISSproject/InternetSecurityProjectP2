﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Word;

namespace InternetSecurityProject.Client
{
    public partial class WriteTXTForm : MetroForm
    {
        InternetSecurityDCDataContext dc = new InternetSecurityDCDataContext();
        public WriteTXTForm()
        {
            InitializeComponent();

            richTextBox1.WordWrap = true;
        }
        string newdata;
        private void metroTextButton1_Click(object sender, EventArgs e)
        {
            //sperate the new input with newline
            using (StreamWriter inStream = new StreamWriter(NameLabel.Text, true))
                inStream.WriteLine();

            using (StreamReader outStream = new StreamReader(NameLabel.Text))
                newdata = outStream.ReadToEnd();
            //Go To the start of richtext appened the file then save them all with the new one
            richTextBox1.SelectionStart = 0;
            richTextBox1.SelectionLength = 0;
            richTextBox1.SelectedText = newdata;

            richTextBox1.SaveFile(NameLabel.Text, RichTextBoxStreamType.UnicodePlainText);

            /*Calculate the New Mac And Update!!*/
            var dbfile = dc.Files.FirstOrDefault(x => x.Name.Equals(NameLabel.Text));

            try
            {
                dbfile.LastUpdateDate = DateTime.Now;
                dbfile.LastUpdateUser = this.IDLabel.Text;
                dc.SubmitChanges();
                var calcMac = new Lib.CMAC();
                calcMac.CreateMAC(dbfile.Directory + @"\" + dbfile.Name);
            }
            catch (Exception ex)
            {
                MsgForm msg = new MsgForm();
                msg.Text = ex.Message.ToString();
                msg.ShowDialog();
            }

            this.Close();
        }
    }
}
