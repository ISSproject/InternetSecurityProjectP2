﻿using MetroFramework.Forms;
using Microsoft.Office.Interop.Word;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace InternetSecurityProject.Client
{
    public partial class WlcForm : MetroForm
    {
        InternetSecurityDCDataContext dc = new InternetSecurityDCDataContext();
        private static WlcForm _instance = new WlcForm();
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private System.Diagnostics.Process process;
        private WlcForm()
        {
            InitializeComponent();
            var obj = LoginForm.GetInstance();
            this.IDLabel.Text = obj.IDBox.Text;

            var dbUser = dc.Users.FirstOrDefault(x => x.Name.Equals(IDLabel.Text));
            this.userSecurityCleareanceLabel.Text = dbUser != null ? dbUser.Level : "Not Set Yet";
            this.userLabelsLabel.Text = dbUser != null ? dbUser.Label : "Not Set Yet";

            InitContent();
        }
        public static WlcForm GetInstance()
        {
            if (_instance == null || _instance.IsDisposed == true)
                _instance = new WlcForm();
            return _instance;
        }
        private void LogoutLink_Click(object sender, EventArgs e)
        {
            var obj = LoginForm.GetInstance();
            obj.Show();
            this.Close();
        }

        private void InitContent()
        {
            var items = this.FileslistBox.Items;
            var obj = new Lib.CModel();
            //Add Matched Files to List
            obj.AddFiles(IDLabel.Text, items);
        }

        private void FileslistBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (FileslistBox.SelectedItem == null)
            {
                this.operationComboBox.Enabled = false;
                return;
            }
            this.operationComboBox.Enabled = true;
            var dbfile = dc.Files.FirstOrDefault(x => x.Name.Equals(FileslistBox.SelectedItem.ToString()));
            this.fileSecurityCleareanceLabel.Text = dbfile != null ? dbfile.Level : "Error";
            this.fileLabelsLabel.Text = dbfile != null ? dbfile.Label : "Error";
            var obj = new Lib.CModel();
            var selectedFile = FileslistBox.SelectedItem.ToString();
            if (obj.IsReadAllowed(selectedFile, IDLabel.Text) == true)
                this.readPicture.Image = Properties.Resources.approve;
            else
                this.readPicture.Image = Properties.Resources.fail;

            if (obj.IsWriteAllowed(selectedFile, IDLabel.Text) == true)
                this.writePicture.Image = Properties.Resources.approve;
            else
                this.writePicture.Image = Properties.Resources.fail;

        }

        private void executeButton_Click(object sender, EventArgs e)
        {
            if (FileslistBox.SelectedItem == null)
            {
                this.resultLabel.Text = "Please Select File First!!";
                return;
            }
            if (operationComboBox.SelectedItem == null)
            {
                this.resultLabel.Text = "Please Select Operation First!!";
                return;
            }
            if (operationComboBox.SelectedItem.ToString() == "Read")
            {
                var model = new Lib.CModel();
                var selectedFile = FileslistBox.SelectedItem.ToString();
                if (model.IsReadAllowed(selectedFile, IDLabel.Text) == false)
                {
                    this.resultLabel.Text = "You Don't Have Privilege To Read This File!!";
                    return;
                }

                logger.Info("User " + this.IDLabel.Text.ToString() + " Request Read Operation For " +
               FileslistBox.SelectedItem.ToString() + " File.");

                var dbfile = dc.Files.FirstOrDefault(x => x.Name.Equals(FileslistBox.SelectedItem.ToString()));
                var obj = new Client.DocViewerForm();
                obj.openDoc(dbfile.Directory + @"\" + dbfile.Name);
                obj.NameLabel.Text = dbfile.Name;
                obj.ShowDialog();

                this.resultLabel.Text = "";
            }
            else //Write Operation
            {
                var model = new Lib.CModel();
                var selectedFile = FileslistBox.SelectedItem.ToString();
                if (model.IsWriteAllowed(selectedFile, IDLabel.Text) == false)
                {
                    this.resultLabel.Text = "You Don't Have Privilege To Write On This File!!";
                    return;
                }
                if (model.IsReadAllowed(selectedFile, IDLabel.Text) == false)
                {// Append Operation "Without" Watching the File Content !!
                    logger.Info("User " + this.IDLabel.Text.ToString() + " Request Appened Operation For " +
                           FileslistBox.SelectedItem.ToString() + " File.");

                    var dbfile = dc.Files.FirstOrDefault(x => x.Name.Equals(FileslistBox.SelectedItem.ToString()));
                    if (Path.GetExtension(dbfile.Name) == ".txt")
                    {
                        var obj = new Client.WriteTXTForm();
                        obj.NameLabel.Text = dbfile.Directory + @"\" + dbfile.Name;
                        obj.IDLabel.Text = this.IDLabel.Text;
                        obj.ShowDialog();
                    }
                    else if (Path.GetExtension(dbfile.Name) == ".docx")
                    {
                        var obj = new Client.WriteDOCForm();
                        obj.NameLabel.Text = dbfile.Directory + @"\" + dbfile.Name;
                        obj.IDLabel.Text = this.IDLabel.Text;
                        obj.ShowDialog();
                    }

                    this.resultLabel.Text = "";
                }
                else //read & write Allowed !! Just Open the File :D :D
                {
                    logger.Info("User " + this.IDLabel.Text.ToString() + " Request Open And Write Operation For " +
                           FileslistBox.SelectedItem.ToString() + " File.");

                    var dbfile = dc.Files.FirstOrDefault(x => x.Name.Equals(FileslistBox.SelectedItem.ToString()));
                    if (Path.GetExtension(dbfile.Name) == ".docx")
                    {
                        /*Template to Open Word File !!*/
                        object missing = System.Reflection.Missing.Value;
                        object readOnly = false;
                        object isVisible = true;
                        object fileName = dbfile.Directory + @"\" + dbfile.Name;
                        ApplicationClass applicationWord = new ApplicationClass();
                        Document modelloBusta = new Document();
                        applicationWord.Visible = true;
                        try
                        {
                            modelloBusta = applicationWord.Documents.Open(ref fileName, ref missing, ref readOnly, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref isVisible, ref missing, ref missing, ref missing, ref missing);
                            modelloBusta.Activate();
                        }
                        catch (COMException eccezione)
                        { //modelloBusta Won't Open for some Crazy reasons -_- !!!
                            var obj = new MsgForm();
                            obj.Text = eccezione.Message;
                            obj.ShowDialog();
                            modelloBusta.Application.Quit(ref missing, ref missing, ref missing);
                        }
                        /*Wait Until the File Closed :D */
                        while (true)
                        {
                            try
                            {// Attempt to open the file exclusively. To check if it's closed or not!!
                                using (FileStream Fs = new FileStream(fileName.ToString(), FileMode.Open, FileAccess.ReadWrite, FileShare.None, 100))
                                    // If we got this far the file is ready and closed :D
                                    break;
                            }
                            catch (IOException)
                            {
                                //wait and retry for the lock to be released for 2 seconds
                                Thread.Sleep(2000);
                            }
                        }
                        /*Calculate the New Mac And Update!!*/
                        try
                        {
                            dbfile.LastUpdateDate = DateTime.Now;
                            dbfile.LastUpdateUser = this.IDLabel.Text;
                            dc.SubmitChanges();
                            var calcMac = new Lib.CMAC();
                            calcMac.CreateMAC(dbfile.Directory + @"\" + dbfile.Name);
                        }
                        catch (Exception ex)
                        {
                            MsgForm msg = new MsgForm();
                            msg.Text = ex.Message.ToString();
                            msg.ShowDialog();
                        }
                    }
                    else if (Path.GetExtension(dbfile.Name) == ".txt")
                    // then it's TXT File :D :D    //EASY PEASY LEMON SQUEEZY
                    {
                        process = System.Diagnostics.Process.Start(dbfile.Directory + @"\" + dbfile.Name);
                        process.WaitForExit();

                        /*Calculate the New Mac And Update!!*/
                        try
                        {
                            dbfile.LastUpdateDate = DateTime.Now;
                            dbfile.LastUpdateUser = this.IDLabel.Text;
                            dc.SubmitChanges();
                            var calcMac = new Lib.CMAC();
                            calcMac.CreateMAC(dbfile.Directory + @"\" + dbfile.Name);
                        }
                        catch (Exception ex)
                        {
                            MsgForm msg = new MsgForm();
                            msg.Text = ex.Message.ToString();
                            msg.ShowDialog();
                        }
                    }
                    this.resultLabel.Text = "";
                }
            }

        }
    }
}