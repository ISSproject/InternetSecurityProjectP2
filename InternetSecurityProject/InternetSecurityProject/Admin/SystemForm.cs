﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InternetSecurityProject.Admin
{
    public partial class SystemForm : MetroForm
    {
        InternetSecurityDCDataContext dc = new InternetSecurityDCDataContext();
        bool error = false;
        public SystemForm()
        {
            InitializeComponent();
            var users = Lib.EnvironmentIdentity.GetComputerUsers();

            if (users != null)
                for (int i = 0; i < users.Count; i++)
                {
                    dataGridView1.Rows.Insert(i, users[i]);
                    if (dc.Users.FirstOrDefault(x => x.Name.Equals(users[i])) != null)
                        dataGridView1.Rows[i].Cells[1].Value = dc.Users.FirstOrDefault(x => x.Name.Equals(users[i])).Level;
                    dataGridView1.Columns["SecurityClassification"].DefaultCellStyle.NullValue = "--Not Set--";
                    dataGridView1.Columns["SecurityClassification"].DefaultCellStyle.DataSourceNullValue = "--Error No DataSource--";
                    if (dc.Users.FirstOrDefault(x => x.Name.Equals(users[i])) != null)
                        dataGridView1.Rows[i].Cells[2].Value = dc.Users.FirstOrDefault(x => x.Name.Equals(users[i])).Label;
                    dataGridView1.Columns["Label"].DefaultCellStyle.NullValue = "--Not Set--";
                    dataGridView1.Columns["Label"].DefaultCellStyle.DataSourceNullValue = "--Error No DataSource--";
                }
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                if (dataGridView1.Rows[i].Cells[1].Value == null || dataGridView1.Rows[i].Cells[2].Value == null)
                {
                    error = true;
                }
                else
                {
                    var user = dc.Users.FirstOrDefault(x => x.Name.Equals(dataGridView1.Rows[i].Cells[0].Value));
                    if (user == null)
                    {
                        User newUser = new User()
                        {
                            Name = dataGridView1.Rows[i].Cells[0].Value.ToString(),
                            Level = dataGridView1.Rows[i].Cells[1].Value.ToString(),
                            Label = dataGridView1.Rows[i].Cells[2].Value.ToString()
                        };
                        dc.Users.InsertOnSubmit(newUser);
                        try
                        {
                            dc.SubmitChanges();
                            error = false;
                        }
                        catch (Exception ex)
                        {
                            MsgForm msg = new MsgForm();
                            msg.Text = ex.Message.ToString();
                            msg.ShowDialog();
                            error = true;
                        }
                    }
                    else
                    {
                        user.Level = dataGridView1.Rows[i].Cells[1].Value.ToString();
                        user.Label = dataGridView1.Rows[i].Cells[2].Value.ToString();
                        try
                        {
                            dc.SubmitChanges();
                            error = false;
                        }
                        catch (Exception ex)
                        {
                            MsgForm msg = new MsgForm();
                            msg.Text = ex.Message.ToString();
                            msg.ShowDialog();
                            error = true;
                        }
                    }
                }
            }
            /**/
            if (!error)
            {
                this.Close();
                var wlcform = Admin.WlcForm.GetInstance();
                wlcform.InitContent(this.DirectoryLabel.Text);
            }
            else if (error)
            {
                MsgForm msg = new MsgForm();
                msg.Text = "Can't processed your request Until You Fill Required Data !";
                msg.ShowDialog();
            }
        }

        private void ChangeFolderButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                this.DirectoryLabel.Text = fbd.SelectedPath;
                this.countlabel.Text = Directory.GetFiles(fbd.SelectedPath, "*.txt").Length.ToString() + "  Text Files";
                this.Sizelabel.Text = GetDirectorySize(fbd.SelectedPath).ToString() + "  KB";
            }
        }


        private static long GetDirectorySize(string p)
        {
            // 1.
            // Get array of all file names.
            string[] a = Directory.GetFiles(p, "*.*");

            // 2.
            // Calculate total bytes of all files in a loop.
            long b = 0;
            foreach (string name in a)
            {
                // 3.
                // Use FileInfo to get length of each file.
                FileInfo info = new FileInfo(name);
                b += info.Length;
            }
            // 4.
            // Return total size
            return b / 1024;
        }

    }
}
