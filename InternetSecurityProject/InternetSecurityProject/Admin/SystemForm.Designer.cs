﻿namespace InternetSecurityProject.Admin
{
    partial class SystemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.systeminfogroupBox = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.UserList = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SecurityClassification = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Label = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.DirectoryLabel = new System.Windows.Forms.Label();
            this.systemuserLabel = new System.Windows.Forms.Label();
            this.labelmainfolder = new System.Windows.Forms.Label();
            this.IDLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ChangeFolderButton = new MetroFramework.Controls.MetroTextBox.MetroTextButton();
            this.OkButton = new MetroFramework.Controls.MetroTextBox.MetroTextButton();
            this.label2 = new System.Windows.Forms.Label();
            this.countlabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Sizelabel = new System.Windows.Forms.Label();
            this.systeminfogroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // systeminfogroupBox
            // 
            this.systeminfogroupBox.Controls.Add(this.Sizelabel);
            this.systeminfogroupBox.Controls.Add(this.label3);
            this.systeminfogroupBox.Controls.Add(this.countlabel);
            this.systeminfogroupBox.Controls.Add(this.label2);
            this.systeminfogroupBox.Controls.Add(this.dataGridView1);
            this.systeminfogroupBox.Controls.Add(this.DirectoryLabel);
            this.systeminfogroupBox.Controls.Add(this.systemuserLabel);
            this.systeminfogroupBox.Controls.Add(this.labelmainfolder);
            this.systeminfogroupBox.Font = new System.Drawing.Font("Tahoma", 10F);
            this.systeminfogroupBox.Location = new System.Drawing.Point(13, 63);
            this.systeminfogroupBox.Name = "systeminfogroupBox";
            this.systeminfogroupBox.Size = new System.Drawing.Size(428, 348);
            this.systeminfogroupBox.TabIndex = 17;
            this.systeminfogroupBox.TabStop = false;
            this.systeminfogroupBox.Text = "System Info";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.HighlightText;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.UserList,
            this.SecurityClassification,
            this.Label});
            this.dataGridView1.Location = new System.Drawing.Point(6, 192);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView1.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridView1.Size = new System.Drawing.Size(416, 142);
            this.dataGridView1.TabIndex = 10;
            // 
            // UserList
            // 
            this.UserList.HeaderText = "User List";
            this.UserList.Name = "UserList";
            // 
            // SecurityClassification
            // 
            this.SecurityClassification.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.SecurityClassification.HeaderText = "Security Classification";
            this.SecurityClassification.Items.AddRange(new object[] {
            "TopSecret",
            "Secret",
            "Confidential",
            "Unclassified"});
            this.SecurityClassification.Name = "SecurityClassification";
            this.SecurityClassification.Width = 145;
            // 
            // Label
            // 
            this.Label.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Label.HeaderText = "Label";
            this.Label.Items.AddRange(new object[] {
            "DOD",
            "NATO",
            "SAS",
            "CIA"});
            this.Label.Name = "Label";
            this.Label.Width = 120;
            // 
            // DirectoryLabel
            // 
            this.DirectoryLabel.AutoSize = true;
            this.DirectoryLabel.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DirectoryLabel.Location = new System.Drawing.Point(174, 30);
            this.DirectoryLabel.Name = "DirectoryLabel";
            this.DirectoryLabel.Size = new System.Drawing.Size(125, 18);
            this.DirectoryLabel.TabIndex = 7;
            this.DirectoryLabel.Text = "Directory Not Set ";
            // 
            // systemuserLabel
            // 
            this.systemuserLabel.AutoSize = true;
            this.systemuserLabel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.systemuserLabel.Location = new System.Drawing.Point(15, 155);
            this.systemuserLabel.Name = "systemuserLabel";
            this.systemuserLabel.Size = new System.Drawing.Size(110, 19);
            this.systemuserLabel.TabIndex = 8;
            this.systemuserLabel.Text = "System Users:";
            // 
            // labelmainfolder
            // 
            this.labelmainfolder.AutoSize = true;
            this.labelmainfolder.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelmainfolder.Location = new System.Drawing.Point(15, 27);
            this.labelmainfolder.Name = "labelmainfolder";
            this.labelmainfolder.Size = new System.Drawing.Size(153, 19);
            this.labelmainfolder.TabIndex = 7;
            this.labelmainfolder.Text = "System Main Folder:";
            // 
            // IDLabel
            // 
            this.IDLabel.AutoSize = true;
            this.IDLabel.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDLabel.Location = new System.Drawing.Point(247, 20);
            this.IDLabel.Name = "IDLabel";
            this.IDLabel.Size = new System.Drawing.Size(56, 17);
            this.IDLabel.TabIndex = 15;
            this.IDLabel.Text = "UserID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label1.Location = new System.Drawing.Point(135, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 17);
            this.label1.TabIndex = 14;
            this.label1.Text = "Welcome ID: ";
            // 
            // ChangeFolderButton
            // 
            this.ChangeFolderButton.Font = new System.Drawing.Font("Tahoma", 12F);
            this.ChangeFolderButton.Image = null;
            this.ChangeFolderButton.Location = new System.Drawing.Point(22, 431);
            this.ChangeFolderButton.Name = "ChangeFolderButton";
            this.ChangeFolderButton.Size = new System.Drawing.Size(110, 38);
            this.ChangeFolderButton.TabIndex = 19;
            this.ChangeFolderButton.Text = "Change Folder";
            this.ChangeFolderButton.UseSelectable = true;
            this.ChangeFolderButton.UseVisualStyleBackColor = true;
            this.ChangeFolderButton.Click += new System.EventHandler(this.ChangeFolderButton_Click);
            // 
            // OkButton
            // 
            this.OkButton.Font = new System.Drawing.Font("Tahoma", 12F);
            this.OkButton.Image = null;
            this.OkButton.Location = new System.Drawing.Point(321, 431);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(110, 38);
            this.OkButton.TabIndex = 20;
            this.OkButton.Text = "Ok";
            this.OkButton.UseSelectable = true;
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label2.Location = new System.Drawing.Point(15, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(194, 19);
            this.label2.TabIndex = 11;
            this.label2.Text = "Directory Folder Contains:";
            // 
            // countlabel
            // 
            this.countlabel.AutoSize = true;
            this.countlabel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.countlabel.Location = new System.Drawing.Point(233, 64);
            this.countlabel.Name = "countlabel";
            this.countlabel.Size = new System.Drawing.Size(48, 19);
            this.countlabel.TabIndex = 12;
            this.countlabel.Text = "count";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label3.Location = new System.Drawing.Point(15, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(161, 19);
            this.label3.TabIndex = 13;
            this.label3.Text = "Directory Folder Size:";
            // 
            // Sizelabel
            // 
            this.Sizelabel.AutoSize = true;
            this.Sizelabel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.Sizelabel.Location = new System.Drawing.Point(219, 107);
            this.Sizelabel.Name = "Sizelabel";
            this.Sizelabel.Size = new System.Drawing.Size(37, 19);
            this.Sizelabel.TabIndex = 14;
            this.Sizelabel.Text = "Size";
            // 
            // SystemForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 487);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.ChangeFolderButton);
            this.Controls.Add(this.IDLabel);
            this.Controls.Add(this.systeminfogroupBox);
            this.Controls.Add(this.label1);
            this.Name = "SystemForm";
            this.systeminfogroupBox.ResumeLayout(false);
            this.systeminfogroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox systeminfogroupBox;
        private System.Windows.Forms.Label systemuserLabel;
        private System.Windows.Forms.Label labelmainfolder;
        public System.Windows.Forms.Label IDLabel;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroTextBox.MetroTextButton ChangeFolderButton;
        private MetroFramework.Controls.MetroTextBox.MetroTextButton OkButton;
        public System.Windows.Forms.Label DirectoryLabel;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn UserList;
        private System.Windows.Forms.DataGridViewComboBoxColumn SecurityClassification;
        private System.Windows.Forms.DataGridViewComboBoxColumn Label;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label countlabel;
        public System.Windows.Forms.Label Sizelabel;
        private System.Windows.Forms.Label label3;
    }
}