﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InternetSecurityProject.Admin
{
    public partial class BackupForm : MetroForm
    {
        InternetSecurityDCDataContext dc = new InternetSecurityDCDataContext();
        public BackupForm()
        {
            InitializeComponent();
        }

        private void ChangeFolderButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                this.DirectoryLabel.Text = fbd.SelectedPath;
            }
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            if (DirectoryLabel.Text == "Directory Not Set ")
            {
                var obj = new MsgForm();
                obj.Text = "Please Specify the Directory First !!";
                obj.ShowDialog();
                return;
            }
            using (StreamWriter outStream = new StreamWriter(DirectoryLabel.Text + @"\backup.txt", true))
            {
                //outStream.WriteByte((byte));
                var dbfilesInfo = dc.Files.ToList();
                outStream.WriteLine(" ----------- INFO BACKUP (" + DateTime.Now + ") -----------");
                outStream.WriteLine();
                foreach (var file in dbfilesInfo)
                {
                    outStream.WriteLine();
                    outStream.WriteLine("FileName:  " + file.Name);
                    outStream.WriteLine("Level:  " + file.Level);
                    outStream.WriteLine("Label:  " + file.Label);
                    outStream.WriteLine("LastUpdateUser:  " + file.LastUpdateUser);
                    outStream.WriteLine("LastUpdateDate:  " + file.LastUpdateDate);

                    byte[] byteKey = file.Key.ToArray();
                    string hexKey = BitConverter.ToString(byteKey).Replace("-", string.Empty);
                    outStream.WriteLine("Key:  " + hexKey);

                    byte[] dataMAC = file.MACHashValue.ToArray();
                    string hexMAC = BitConverter.ToString(dataMAC).Replace("-", string.Empty);
                    outStream.WriteLine("MACHashValue:  " + hexMAC);
                    outStream.WriteLine("-------------------------------------");
                }
            }

            this.Close();
        }


        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }
    }
}
