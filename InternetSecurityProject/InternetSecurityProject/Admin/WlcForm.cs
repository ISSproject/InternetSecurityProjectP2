﻿using MetroFramework.Forms;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System;
using System.IO;
using MetroFramework.Controls;
using System.Runtime.InteropServices;

//TODo: Add backup log button  or form !!
//TODO: add verfily status for each file !  TRue or False

namespace InternetSecurityProject.Admin
{
    public partial class WlcForm : MetroForm
    {
        string MainFolderPath = @"C:\server\";
        InternetSecurityDCDataContext dc = new InternetSecurityDCDataContext();
        private static WlcForm _instance = new WlcForm();
        bool error = false;
        private WlcForm()
        {
            InitializeComponent();
            /**/
            InitContent(MainFolderPath);
        }
        public static WlcForm GetInstance()
        {
            if (_instance == null || _instance.IsDisposed == true)
                _instance = new WlcForm();
            return _instance;
        }
        public void InitContent(string path)
        {
            DirectoryInfo dinfo = new DirectoryInfo(path);
            FileInfo[] Files = dinfo.GetFiles("*.txt");

            dataGridView1.Rows.Clear();
            for (int i = 0; i < Files.Length; i++)
            {
                dataGridView1.Rows.Insert(i, Files[i].Name);
                if (dc.Files.FirstOrDefault(x => x.Name.Equals(Files[i].Name)) != null)
                    dataGridView1.Rows[i].Cells[1].Value = dc.Files.FirstOrDefault(x => x.Name.Equals(Files[i].Name)).Level;
                dataGridView1.Columns["SecurityLevel"].DefaultCellStyle.NullValue = "--Select A Level--";
                dataGridView1.Columns["SecurityLevel"].DefaultCellStyle.DataSourceNullValue = "--Error No DataSource--";
                if (dc.Files.FirstOrDefault(x => x.Name.Equals(Files[i].Name)) != null)
                    dataGridView1.Rows[i].Cells[2].Value = dc.Files.FirstOrDefault(x => x.Name.Equals(Files[i].Name)).Label;
                dataGridView1.Columns["Label"].DefaultCellStyle.NullValue = "--Select A Category--";
                dataGridView1.Columns["Label"].DefaultCellStyle.DataSourceNullValue = "--Error No DataSource--";
            }
            var newcounter = Files.Length ;
            Files = dinfo.GetFiles("*.docx");
            for (int i = 0; i < Files.Length; i++)
            {
                dataGridView1.Rows.Insert(i + newcounter, Files[i].Name);
                if (dc.Files.FirstOrDefault(x => x.Name.Equals(Files[i].Name)) != null)
                    dataGridView1.Rows[i + newcounter].Cells[1].Value = dc.Files.FirstOrDefault(x => x.Name.Equals(Files[i].Name)).Level;
                dataGridView1.Columns["SecurityLevel"].DefaultCellStyle.NullValue = "--Select A Level--";
                dataGridView1.Columns["SecurityLevel"].DefaultCellStyle.DataSourceNullValue = "--Error No DataSource--";
                if (dc.Files.FirstOrDefault(x => x.Name.Equals(Files[i].Name)) != null)
                    dataGridView1.Rows[i + newcounter].Cells[2].Value = dc.Files.FirstOrDefault(x => x.Name.Equals(Files[i].Name)).Label;
                dataGridView1.Columns["Label"].DefaultCellStyle.NullValue = "--Select A Category--";
                dataGridView1.Columns["Label"].DefaultCellStyle.DataSourceNullValue = "--Error No DataSource--";
            }
        }
        private void SysInfoButton_Click(object sender, EventArgs e)
        {
            var systemform = new Admin.SystemForm();
            systemform.DirectoryLabel.Text = this.MainFolderPath;
            systemform.IDLabel.Text = this.IDLabel.Text;
            systemform.countlabel.Text = Directory.GetFiles(MainFolderPath, "*.txt").Length.ToString() + "  Text Files";
            systemform.Sizelabel.Text = GetDirectorySize(MainFolderPath).ToString() + "  KB";
            systemform.ShowDialog();
        }
        private void LogoutLink_Click(object sender, EventArgs e)
        {
            var obj = LoginForm.GetInstance();
            obj.Show();
            this.Close();
        }
        private void ChangeFolderButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                MainFolderPath = fbd.SelectedPath;
                //dc.Files.DeleteAllOnSubmit(dc.Files);
                InitContent(MainFolderPath);
            }
        }
        private void ExecuteButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                if (dataGridView1.Rows[i].Cells[1].Value == null || dataGridView1.Rows[i].Cells[2].Value == null)
                {
                    error = true;
                }
                else
                {
                    var file = dc.Files.FirstOrDefault(x => x.Name.Equals(dataGridView1.Rows[i].Cells[0].Value));
                    if (file == null)
                    {
                        File newFile = new File()
                        {
                            Name = dataGridView1.Rows[i].Cells[0].Value.ToString(),
                            Level = dataGridView1.Rows[i].Cells[1].Value.ToString(),
                            Label = dataGridView1.Rows[i].Cells[2].Value.ToString(),
                            LastUpdateDate = DateTime.Now,
                            LastUpdateUser = this.IDLabel.Text,
                            Directory = MainFolderPath
                        };
                        dc.Files.InsertOnSubmit(newFile);
                        try
                        {
                            dc.SubmitChanges();
                            var calcMac = new Lib.CMAC();
                            calcMac.CreateMAC(MainFolderPath + @"\" + newFile.Name);
                            error = false;
                        }
                        catch (Exception ex)
                        {
                            MsgForm msg = new MsgForm();
                            msg.Text = ex.Message.ToString();
                            msg.ShowDialog();
                            error = true;
                        }
                    }
                    else
                    {
                        file.Level = dataGridView1.Rows[i].Cells[1].Value.ToString();
                        file.Label = dataGridView1.Rows[i].Cells[2].Value.ToString();
                        file.LastUpdateDate = DateTime.Now;
                        file.LastUpdateUser = this.IDLabel.Text;
                        file.Directory = MainFolderPath;
                        try
                        {
                            dc.SubmitChanges();
                            var calcMac = new Lib.CMAC();
                            calcMac.CreateMAC(MainFolderPath + @"\" + file.Name);
                            error = false;
                        }
                        catch (Exception ex)
                        {
                            MsgForm msg = new MsgForm();
                            msg.Text = ex.Message.ToString();
                            msg.ShowDialog();
                            error = true;
                        }
                    }
                }
            }
            /**/
            if (!error)
            {
                MsgForm msg = new MsgForm();
                msg.Text = "Operation Done !!";
                msg.ShowDialog();
            }
            else if (error)
            {
                MsgForm msg = new MsgForm();
                msg.Text = "Can't processed your request Until You Fill Required Data !";
                msg.ShowDialog();
            }
        }

        private void backupButton_Click(object sender, EventArgs e)
        {
            var backupform = new Admin.BackupForm();
            backupform.ShowDialog();
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            var currentcell = dataGridView1.CurrentCellAddress;
            var file = dataGridView1.Rows[currentcell.Y].Cells[0].Value;
            var dbfile = dc.Files.FirstOrDefault(x => x.Name.Equals(file));
            this.LastUpdateBylabel.Text = dbfile != null ? dbfile.LastUpdateUser : "";
            this.LastUpdateDatelabel.Text = dbfile != null ? dbfile.LastUpdateDate.ToString() : "";
        }

        static long GetDirectorySize(string p)
        {
            // 1.
            // Get array of all file names.
            string[] a = Directory.GetFiles(p, "*.*");

            // 2.
            // Calculate total bytes of all files in a loop.
            long b = 0;
            foreach (string name in a)
            {
                // 3.
                // Use FileInfo to get length of each file.
                FileInfo info = new FileInfo(name);
                b += info.Length;
            }
            // 4.
            // Return total size
            return b / 1024;
        }
    }
}
