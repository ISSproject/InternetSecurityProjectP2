﻿namespace InternetSecurityProject.Admin
{
    partial class BackupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OkButton = new MetroFramework.Controls.MetroTextBox.MetroTextButton();
            this.ChangeFolderButton = new MetroFramework.Controls.MetroTextBox.MetroTextButton();
            this.systeminfogroupBox = new System.Windows.Forms.GroupBox();
            this.DirectoryLabel = new System.Windows.Forms.Label();
            this.labelmainfolder = new System.Windows.Forms.Label();
            this.systeminfogroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // OkButton
            // 
            this.OkButton.Font = new System.Drawing.Font("Tahoma", 12F);
            this.OkButton.Image = null;
            this.OkButton.Location = new System.Drawing.Point(322, 148);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(110, 38);
            this.OkButton.TabIndex = 25;
            this.OkButton.Text = "Ok";
            this.OkButton.UseSelectable = true;
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // ChangeFolderButton
            // 
            this.ChangeFolderButton.Font = new System.Drawing.Font("Tahoma", 12F);
            this.ChangeFolderButton.Image = null;
            this.ChangeFolderButton.Location = new System.Drawing.Point(23, 148);
            this.ChangeFolderButton.Name = "ChangeFolderButton";
            this.ChangeFolderButton.Size = new System.Drawing.Size(110, 38);
            this.ChangeFolderButton.TabIndex = 24;
            this.ChangeFolderButton.Text = "Set Directory";
            this.ChangeFolderButton.UseSelectable = true;
            this.ChangeFolderButton.UseVisualStyleBackColor = true;
            this.ChangeFolderButton.Click += new System.EventHandler(this.ChangeFolderButton_Click);
            // 
            // systeminfogroupBox
            // 
            this.systeminfogroupBox.Controls.Add(this.DirectoryLabel);
            this.systeminfogroupBox.Controls.Add(this.labelmainfolder);
            this.systeminfogroupBox.Font = new System.Drawing.Font("Tahoma", 10F);
            this.systeminfogroupBox.Location = new System.Drawing.Point(14, 28);
            this.systeminfogroupBox.Name = "systeminfogroupBox";
            this.systeminfogroupBox.Size = new System.Drawing.Size(428, 96);
            this.systeminfogroupBox.TabIndex = 23;
            this.systeminfogroupBox.TabStop = false;
            this.systeminfogroupBox.Text = "System Backup Directory";
            // 
            // DirectoryLabel
            // 
            this.DirectoryLabel.AutoSize = true;
            this.DirectoryLabel.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DirectoryLabel.Location = new System.Drawing.Point(174, 41);
            this.DirectoryLabel.Name = "DirectoryLabel";
            this.DirectoryLabel.Size = new System.Drawing.Size(125, 18);
            this.DirectoryLabel.TabIndex = 7;
            this.DirectoryLabel.Text = "Directory Not Set ";
            // 
            // labelmainfolder
            // 
            this.labelmainfolder.AutoSize = true;
            this.labelmainfolder.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelmainfolder.Location = new System.Drawing.Point(15, 41);
            this.labelmainfolder.Name = "labelmainfolder";
            this.labelmainfolder.Size = new System.Drawing.Size(113, 19);
            this.labelmainfolder.TabIndex = 7;
            this.labelmainfolder.Text = "Backup Driver:";
            // 
            // BackupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 204);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.ChangeFolderButton);
            this.Controls.Add(this.systeminfogroupBox);
            this.Name = "BackupForm";
            this.systeminfogroupBox.ResumeLayout(false);
            this.systeminfogroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox.MetroTextButton OkButton;
        private MetroFramework.Controls.MetroTextBox.MetroTextButton ChangeFolderButton;
        private System.Windows.Forms.GroupBox systeminfogroupBox;
        public System.Windows.Forms.Label DirectoryLabel;
        private System.Windows.Forms.Label labelmainfolder;
    }
}