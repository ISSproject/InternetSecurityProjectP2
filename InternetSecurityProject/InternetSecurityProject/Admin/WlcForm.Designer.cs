﻿namespace InternetSecurityProject.Admin
{
    partial class WlcForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.filesgroupBox = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.FilesName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SecurityLevel = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Label = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LogoutLink = new MetroFramework.Controls.MetroLink();
            this.IDLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ChangeFolderButton = new MetroFramework.Controls.MetroTextBox.MetroTextButton();
            this.SysInfoButton = new MetroFramework.Controls.MetroTextBox.MetroTextButton();
            this.ExecuteButton = new MetroFramework.Controls.MetroTextBox.MetroTextButton();
            this.FileInfogroupBox = new System.Windows.Forms.GroupBox();
            this.LastUpdateDatelabel = new System.Windows.Forms.Label();
            this.LastUpdateBylabel = new System.Windows.Forms.Label();
            this.Lastlabel2 = new System.Windows.Forms.Label();
            this.Lastlabel1 = new System.Windows.Forms.Label();
            this.backupButton = new MetroFramework.Controls.MetroTextBox.MetroTextButton();
            this.filesgroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.FileInfogroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // filesgroupBox
            // 
            this.filesgroupBox.Controls.Add(this.dataGridView1);
            this.filesgroupBox.Location = new System.Drawing.Point(23, 43);
            this.filesgroupBox.Name = "filesgroupBox";
            this.filesgroupBox.Size = new System.Drawing.Size(862, 374);
            this.filesgroupBox.TabIndex = 15;
            this.filesgroupBox.TabStop = false;
            this.filesgroupBox.Text = "File System";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FilesName,
            this.SecurityLevel,
            this.Label});
            this.dataGridView1.Location = new System.Drawing.Point(0, 19);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.DefaultCellStyle.Padding = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.dataGridView1.RowTemplate.Height = 25;
            this.dataGridView1.Size = new System.Drawing.Size(862, 355);
            this.dataGridView1.TabIndex = 2;
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // FilesName
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.FilesName.DefaultCellStyle = dataGridViewCellStyle6;
            this.FilesName.HeaderText = "Files Name";
            this.FilesName.Name = "FilesName";
            this.FilesName.ReadOnly = true;
            this.FilesName.Width = 400;
            // 
            // SecurityLevel
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.Padding = new System.Windows.Forms.Padding(0, 1, 1, 0);
            this.SecurityLevel.DefaultCellStyle = dataGridViewCellStyle7;
            this.SecurityLevel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.SecurityLevel.HeaderText = "Security Level";
            this.SecurityLevel.Items.AddRange(new object[] {
            "TopSecret",
            "Secret",
            "Confidential",
            "Unclassified"});
            this.SecurityLevel.Name = "SecurityLevel";
            this.SecurityLevel.Width = 200;
            // 
            // Label
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.Label.DefaultCellStyle = dataGridViewCellStyle8;
            this.Label.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Label.HeaderText = "Label";
            this.Label.Items.AddRange(new object[] {
            "DOD",
            "NATO",
            "SAS",
            "CIA"});
            this.Label.Name = "Label";
            this.Label.Width = 200;
            // 
            // LogoutLink
            // 
            this.LogoutLink.FontSize = MetroFramework.MetroLinkSize.Medium;
            this.LogoutLink.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.LogoutLink.Location = new System.Drawing.Point(810, 500);
            this.LogoutLink.Name = "LogoutLink";
            this.LogoutLink.Size = new System.Drawing.Size(75, 23);
            this.LogoutLink.Style = MetroFramework.MetroColorStyle.Blue;
            this.LogoutLink.TabIndex = 14;
            this.LogoutLink.Text = "LOGOUT";
            this.LogoutLink.Theme = MetroFramework.MetroThemeStyle.Light;
            this.LogoutLink.UseSelectable = true;
            this.LogoutLink.Click += new System.EventHandler(this.LogoutLink_Click);
            // 
            // IDLabel
            // 
            this.IDLabel.AutoSize = true;
            this.IDLabel.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDLabel.Location = new System.Drawing.Point(471, 14);
            this.IDLabel.Name = "IDLabel";
            this.IDLabel.Size = new System.Drawing.Size(56, 17);
            this.IDLabel.TabIndex = 13;
            this.IDLabel.Text = "UserID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label1.Location = new System.Drawing.Point(359, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 17);
            this.label1.TabIndex = 12;
            this.label1.Text = "Welcome ID: ";
            // 
            // ChangeFolderButton
            // 
            this.ChangeFolderButton.Font = new System.Drawing.Font("Tahoma", 12F);
            this.ChangeFolderButton.Image = null;
            this.ChangeFolderButton.Location = new System.Drawing.Point(399, 431);
            this.ChangeFolderButton.Name = "ChangeFolderButton";
            this.ChangeFolderButton.Size = new System.Drawing.Size(110, 38);
            this.ChangeFolderButton.TabIndex = 18;
            this.ChangeFolderButton.Text = "Change Folder";
            this.ChangeFolderButton.UseSelectable = true;
            this.ChangeFolderButton.UseVisualStyleBackColor = true;
            this.ChangeFolderButton.Click += new System.EventHandler(this.ChangeFolderButton_Click);
            // 
            // SysInfoButton
            // 
            this.SysInfoButton.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SysInfoButton.Image = null;
            this.SysInfoButton.Location = new System.Drawing.Point(529, 464);
            this.SysInfoButton.Name = "SysInfoButton";
            this.SysInfoButton.Size = new System.Drawing.Size(110, 38);
            this.SysInfoButton.TabIndex = 19;
            this.SysInfoButton.Text = "System Info";
            this.SysInfoButton.UseSelectable = true;
            this.SysInfoButton.UseVisualStyleBackColor = true;
            this.SysInfoButton.Click += new System.EventHandler(this.SysInfoButton_Click);
            // 
            // ExecuteButton
            // 
            this.ExecuteButton.Font = new System.Drawing.Font("Tahoma", 12F);
            this.ExecuteButton.Image = null;
            this.ExecuteButton.Location = new System.Drawing.Point(655, 464);
            this.ExecuteButton.Name = "ExecuteButton";
            this.ExecuteButton.Size = new System.Drawing.Size(110, 38);
            this.ExecuteButton.TabIndex = 20;
            this.ExecuteButton.Text = "Execute";
            this.ExecuteButton.UseSelectable = true;
            this.ExecuteButton.UseVisualStyleBackColor = true;
            this.ExecuteButton.Click += new System.EventHandler(this.ExecuteButton_Click);
            // 
            // FileInfogroupBox
            // 
            this.FileInfogroupBox.Controls.Add(this.LastUpdateDatelabel);
            this.FileInfogroupBox.Controls.Add(this.LastUpdateBylabel);
            this.FileInfogroupBox.Controls.Add(this.Lastlabel2);
            this.FileInfogroupBox.Controls.Add(this.Lastlabel1);
            this.FileInfogroupBox.Location = new System.Drawing.Point(23, 423);
            this.FileInfogroupBox.Name = "FileInfogroupBox";
            this.FileInfogroupBox.Size = new System.Drawing.Size(338, 100);
            this.FileInfogroupBox.TabIndex = 21;
            this.FileInfogroupBox.TabStop = false;
            this.FileInfogroupBox.Text = "File Info";
            // 
            // LastUpdateDatelabel
            // 
            this.LastUpdateDatelabel.AutoSize = true;
            this.LastUpdateDatelabel.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LastUpdateDatelabel.Location = new System.Drawing.Point(135, 62);
            this.LastUpdateDatelabel.Name = "LastUpdateDatelabel";
            this.LastUpdateDatelabel.Size = new System.Drawing.Size(0, 18);
            this.LastUpdateDatelabel.TabIndex = 25;
            // 
            // LastUpdateBylabel
            // 
            this.LastUpdateBylabel.AutoSize = true;
            this.LastUpdateBylabel.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LastUpdateBylabel.Location = new System.Drawing.Point(135, 28);
            this.LastUpdateBylabel.Name = "LastUpdateBylabel";
            this.LastUpdateBylabel.Size = new System.Drawing.Size(0, 18);
            this.LastUpdateBylabel.TabIndex = 24;
            // 
            // Lastlabel2
            // 
            this.Lastlabel2.AutoSize = true;
            this.Lastlabel2.Font = new System.Drawing.Font("Tahoma", 10F);
            this.Lastlabel2.Location = new System.Drawing.Point(6, 62);
            this.Lastlabel2.Name = "Lastlabel2";
            this.Lastlabel2.Size = new System.Drawing.Size(123, 17);
            this.Lastlabel2.TabIndex = 23;
            this.Lastlabel2.Text = "Last Update Date: ";
            // 
            // Lastlabel1
            // 
            this.Lastlabel1.AutoSize = true;
            this.Lastlabel1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.Lastlabel1.Location = new System.Drawing.Point(6, 28);
            this.Lastlabel1.Name = "Lastlabel1";
            this.Lastlabel1.Size = new System.Drawing.Size(110, 17);
            this.Lastlabel1.TabIndex = 22;
            this.Lastlabel1.Text = "Last Update By: ";
            // 
            // backupButton
            // 
            this.backupButton.Font = new System.Drawing.Font("Tahoma", 12F);
            this.backupButton.Image = null;
            this.backupButton.Location = new System.Drawing.Point(399, 485);
            this.backupButton.Name = "backupButton";
            this.backupButton.Size = new System.Drawing.Size(110, 38);
            this.backupButton.TabIndex = 22;
            this.backupButton.Text = "Backup Keys";
            this.backupButton.UseSelectable = true;
            this.backupButton.UseVisualStyleBackColor = true;
            this.backupButton.Click += new System.EventHandler(this.backupButton_Click);
            // 
            // WlcForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(908, 529);
            this.Controls.Add(this.backupButton);
            this.Controls.Add(this.FileInfogroupBox);
            this.Controls.Add(this.ExecuteButton);
            this.Controls.Add(this.SysInfoButton);
            this.Controls.Add(this.ChangeFolderButton);
            this.Controls.Add(this.filesgroupBox);
            this.Controls.Add(this.LogoutLink);
            this.Controls.Add(this.IDLabel);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "WlcForm";
            this.filesgroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.FileInfogroupBox.ResumeLayout(false);
            this.FileInfogroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox filesgroupBox;
        private MetroFramework.Controls.MetroLink LogoutLink;
        public System.Windows.Forms.Label IDLabel;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroTextBox.MetroTextButton ChangeFolderButton;
        private MetroFramework.Controls.MetroTextBox.MetroTextButton SysInfoButton;
        private System.Windows.Forms.DataGridView dataGridView1;
        private MetroFramework.Controls.MetroTextBox.MetroTextButton ExecuteButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn FilesName;
        private System.Windows.Forms.DataGridViewComboBoxColumn SecurityLevel;
        private System.Windows.Forms.DataGridViewComboBoxColumn Label;
        private System.Windows.Forms.GroupBox FileInfogroupBox;
        private System.Windows.Forms.Label Lastlabel2;
        private System.Windows.Forms.Label Lastlabel1;
        private System.Windows.Forms.Label LastUpdateDatelabel;
        private System.Windows.Forms.Label LastUpdateBylabel;
        private MetroFramework.Controls.MetroTextBox.MetroTextButton backupButton;
    }
}