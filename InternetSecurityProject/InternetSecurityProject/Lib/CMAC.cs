﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace InternetSecurityProject.Lib
{
    public class CMAC
    {
        InternetSecurityDCDataContext dc = new InternetSecurityDCDataContext();
        public CMAC()
        {
            //var dbfile = new File()
            //{
            //    Name = "DeltaForce_mission_briefing.txt",
            //    Label = "test",
            //    LastUpdateDate = DateTime.Now,
            //    LastUpdateUser = "Admin",
            //    Level = "test"
            //};
            //dc.Files.InsertOnSubmit(dbfile);
            //dc.SubmitChanges();
            //CreateMAC(@"C:\server\DeltaForce_mission_briefing.txt");
            //VerifyFile(@"C:\server\DeltaForce_mission_briefing.txt");
        }
        public void CreateMAC(string dataFile)
        {
            byte[] secretkey = new Byte[24];
            //RNGCryptoServiceProvider is an implementation of a random number generator.
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {   // The array is now filled with cryptographically strong random bytes.
                rng.GetBytes(secretkey);
                // Use the secret key to sign the file.
                SignFile(secretkey, dataFile);
            }
        }

        // Computes a keyed hash for a source file and creates a target file with the keyed hash
        // prepended to the contents of the source file. 
        private void SignFile(byte[] key, String sourceFile)
        {
            // Initialize the keyed hash object.
            using (MACTripleDES hmac = new MACTripleDES(key))
            using (FileStream inStream = new FileStream(sourceFile, FileMode.Open))
            {
                // Compute the hash of the input file.
                byte[] hashValue = hmac.ComputeHash(inStream);
                // Reset inStream to the beginning of the file.
                var dbfile = dc.Files.FirstOrDefault(x => x.Name.Equals(Path.GetFileName(sourceFile)));
                dbfile.Key = key;
                dbfile.MACHashValue = hashValue;
                dc.SubmitChanges();
            }
            return;
        } // end SignFile

        // Compares the key in the source file with a new key created for the data portion of the file. If the keys 
        // compare the data has not been tampered with.
        public bool VerifyFile(String sourceFile)
        {
            bool err = false;
            var dbfile = dc.Files.FirstOrDefault(x => x.Name.Equals(Path.GetFileName(sourceFile)));
            var dbKey = dbfile.Key.ToArray();
            // Initialize the keyed hash object. 
            using (MACTripleDES hmac = new MACTripleDES(dbKey))
            {
                // Create an array to hold the keyed hash value read from the file.
                byte[] storedHash = new byte[hmac.HashSize / 8];
                // Create a FileStream for the source file.
                using (FileStream inStream = new FileStream(sourceFile, FileMode.Open))
                {
                    // Read in the storedHash.
                    storedHash = dbfile.MACHashValue.ToArray();
                    // Compute the hash of the remaining contents of the file.
                    byte[] computedHash = hmac.ComputeHash(inStream);
                    // compare the computed hash with the stored value
                    for (int i = 0; i < storedHash.Length; i++)
                        if (computedHash[i] != storedHash[i])
                        {
                            err = true;
                        }
                }
            }
            if (err)
            {
                Console.WriteLine("Hash values differ! Signed file has been tampered with!");
                return false;
            }
            else
            {
                Console.WriteLine("Hash values agree -- no tampering occurred.");
                return true;
            }

        } //end VerifyFile

    }
}
