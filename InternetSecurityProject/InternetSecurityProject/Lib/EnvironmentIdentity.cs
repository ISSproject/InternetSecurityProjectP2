﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetSecurityProject.Lib
{
    class EnvironmentIdentity
    {
        [System.Runtime.InteropServices.DllImport("advapi32.dll")]
        public static extern bool LogonUser(string userName, string domainName, string password, int LogonType, int LogonProvider, ref IntPtr phToken);
        public static bool GetCurrentEnvironment(string id, string pass)
        {
            System.Security.Principal.WindowsIdentity currentUser = System.Security.Principal.WindowsIdentity.GetCurrent();
            
            bool isValid = IsValidateCredentials(id, pass, "");
            return isValid;
        }
        private static bool IsValidateCredentials(string userName, string password, string domain)
        {
            IntPtr tokenHandler = IntPtr.Zero;
            bool isValid = LogonUser(userName, domain, password, 2, 0, ref tokenHandler);
            return isValid;
        }
        public static string GetCurrentUser()
        {
            var currentUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            return currentUser.Substring(currentUser.IndexOf(@"\") + 1);
        }

        public static List<string> GetComputerUsers()
        {
            List<string> users = new List<string>();
            var path = string.Format("WinNT://{0},computer", Environment.MachineName);

            using (var computerEntry = new DirectoryEntry(path))
                foreach (DirectoryEntry childEntry in computerEntry.Children)
                    if (childEntry.SchemaClassName == "User" &&
                        childEntry.Name != "Administrator" &&
                        childEntry.Name != "Guest")

                        users.Add(childEntry.Name);
            return users;
        }
    }
}
