﻿using MetroFramework.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InternetSecurityProject.Lib
{
    public class CModel
    {
        InternetSecurityDCDataContext dc = new InternetSecurityDCDataContext();
        List<string> TopSecretReadList = new List<string>
        {"TopSecret","Secret","Confidential","Unclassified"};
        List<string> SecretReadList = new List<string>
        {"Secret","Confidential","Unclassified"};
        List<string> ConfidentialReadList = new List<string>
        {"Confidential","Unclassified"};
        List<string> SecretWriteList = new List<string>
        {"TopSecret","Secret"};
        List<string> ConfidentialWriteList = new List<string>
        {"TopSecret","Secret","Confidential"};
        List<string> UnclassifiedWriteList = new List<string>
        {"TopSecret","Secret","Confidential","Unclassified"};

        public void GetLevels(MetroComboBox bx)
        {
            bx.Items.Clear();
            bx.Items.Add("--Select a Level--");
            bx.Items.Add("TopSecret");
            bx.Items.Add("Secret");
            bx.Items.Add("Confidential");
            bx.Items.Add("Unclassified");
        }
        //((DataGridViewComboBoxColumn)dataGridView1.Columns["Column2"]).DataSource = CB.Items;

        public void AddFiles(string UserName, ListBox.ObjectCollection ListItems)
        {
            var dbfiles = dc.Files.ToList();
            var dbUser = dc.Users.FirstOrDefault(x => x.Name.Equals(UserName));
            if (dbUser != null)
            {
                var userLabel = dbUser.Label;
                foreach (var file in dbfiles)
                    if (file.Label == userLabel)
                        ListItems.Add(file.Name);
            }
        }
        public bool IsReadAllowed(string File, string UserName)
        {
            var dbfile = dc.Files.FirstOrDefault(x => x.Name.Equals(File));
            var dbUser = dc.Users.FirstOrDefault(x => x.Name.Equals(UserName));
            if (dbUser == null)
                return false;

            var userCleareance = dbUser.Level;
            switch (userCleareance)
            {
                case "TopSecret":
                    if (TopSecretReadList.Exists(e => e.Equals(dbfile.Level)))
                        return true;
                    else
                        return false;
                case "Secret":
                    if (SecretReadList.Exists(e => e.Equals(dbfile.Level)))
                        return true;
                    else
                        return false;
                case "Confidential":
                    if (ConfidentialReadList.Exists(e => e.Equals(dbfile.Level)))
                        return true;
                    else
                        return false;
                case "Unclassified":
                    if (userCleareance == dbfile.Level)
                        return true;
                    else
                        return false;
                default:
                    return false;
            }
        }
        public bool IsWriteAllowed(string File, string UserName)
        {
            var dbfile = dc.Files.FirstOrDefault(x => x.Name.Equals(File));
            var dbUser = dc.Users.FirstOrDefault(x => x.Name.Equals(UserName));
            if (dbUser == null)
                return false;

            var userCleareance = dbUser.Level;
            switch (userCleareance)
            {
                case "TopSecret":
                    if (userCleareance == dbfile.Level)
                        return true;
                    else
                        return false;
                case "Secret":
                    if (SecretWriteList.Exists(e => e.Equals(dbfile.Level)))
                        return true;
                    else
                        return false;
                case "Confidential":
                    if (ConfidentialWriteList.Exists(e => e.Equals(dbfile.Level)))
                        return true;
                    else
                        return false;
                case "Unclassified":
                    if (UnclassifiedWriteList.Exists(e => e.Equals(dbfile.Level)))
                        return true;
                    else
                        return false;
                default:
                    return false;
            }
        }


    }


}

