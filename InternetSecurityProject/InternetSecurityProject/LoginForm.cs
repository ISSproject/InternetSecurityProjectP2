﻿using MetroFramework.Forms;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InternetSecurityProject
{
    public partial class LoginForm : MetroForm
    {
        private static readonly LoginForm _instance = new LoginForm();
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private LoginForm()
        {
            InitializeComponent();
            this.IDBox.Text = Lib.EnvironmentIdentity.GetCurrentUser();
            this.ActiveControl = this.PassBox;
            PassBox.Select();
        }
        public static LoginForm GetInstance()
        {
            return _instance;
        }
        private void LoginButton_Click(object sender, EventArgs e)
        {
            if (IDBox.Text.Equals("root") && PassBox.Text.Equals("root"))
            {
                var wlcform = Admin.WlcForm.GetInstance();
                wlcform.IDLabel.Text = "Administrator";
                wlcform.Show();
                this.Hide();
            }
            else if (Lib.EnvironmentIdentity.GetCurrentEnvironment(this.IDBox.Text.ToString(), this.PassBox.Text.ToString()))
            {
                var wlcform = Client.WlcForm.GetInstance();
                wlcform.Show();
                logger.Info("User " + IDBox.Text.ToString() + " SignedIn !");
                this.Hide();
            }
            else
            {
                var obj = new MsgForm();
                obj.Text = "Invalid User Details";
                obj.Show();
                logger.Info("Faild Attempted To SignIn with UserId  " + IDBox.Text.ToString());
                this.LoginErrorLabel.Visible = true;
            }

        }

        // Check for Enter input!
        private void CheckKeys(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                LoginButton_Click(sender, e);
                e.Handled = true;
            }
        }

    }
}
